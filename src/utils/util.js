
import Store from '../store';
// import locale from '../locale/locale';

export {
    mouseclickposition,
    luckysheetfontformat,
    common_extend,
    getObjType,
    chatatABC,
    rgbTohex,
    ABCatNum,
    replaceHtml,
    showrightclickmenu,
}

//二级菜单显示位置
function mouseclickposition($menu, x, y, p) {
    let winH = $(window).height(), winW = $(window).width();
    let menuW = $menu.width(), menuH = $menu.height();

    if (p == null) {
        p = "lefttop";
    }

    if (p == "lefttop") {
        $menu.css({ "top": y, "left": x }).show();
    }
    else if (p == "righttop") {
        $menu.css({ "top": y, "left": x - menuW }).show();
    }
    else if (p == "leftbottom") {
        $menu.css({ "bottom": winH - y - 12, "left": x }).show();
    }
    else if (p == "rightbottom") {
        $menu.css({ "bottom": winH - y - 12, "left": x - menuW }).show();
    }
}

function luckysheetfontformat(format) {
    // let fontarray = locale().fontarray;
        let font = "normal ";//font-variant

        if (format.it == "1") { //font-style
            font += "italic ";
        }
        if (format.bl == "1") {
            font += "bold ";
        }

        if (!format.fs) {   //font-size/line-height
            font += Store.defaultFontSize + "pt ";
        }
        else {
            font += Math.ceil(format.fs) + "pt ";
        }
        font += ' Times New Roman';

        return font;

}
/**
 * Common tool methods
 */
// replace temp ${xxx} to dataarry ,temp:String，This is html ，dataarry：an object{"xxx":"String for replace"}
// e.g.：luckysheet.replaceHtml("${image}",{"image":"abc","jskdjslf":"abc"})   ==>  abc
function replaceHtml(temp, dataarry) {
    return temp.replace(/\$\{([\w]+)\}/g, function (s1, s2) { let s = dataarry[s2]; if (typeof (s) != "undefined") { return s; } else { return s1; } });
};

/**
 * extend two objects
 * @param {Object } jsonbject1
 * @param {Object } jsonbject2 
 */
function common_extend(jsonbject1, jsonbject2) {
    let resultJsonObject = {};

    for (let attr in jsonbject1) {
        resultJsonObject[attr] = jsonbject1[attr];
    }

    for (let attr in jsonbject2) {
        // undefined is equivalent to no setting
        if(jsonbject2[attr] == undefined){
            continue;
        }
        resultJsonObject[attr] = jsonbject2[attr];
    }

    return resultJsonObject;
}


//获取数据类型
function getObjType(obj) {
    let toString = Object.prototype.toString;

    let map = {
        '[object Boolean]': 'boolean',
        '[object Number]': 'number',
        '[object String]': 'string',
        '[object Function]': 'function',
        '[object Array]': 'array',
        '[object Date]': 'date',
        '[object RegExp]': 'regExp',
        '[object Undefined]': 'undefined',
        '[object Null]': 'null',
        '[object Object]': 'object'
    }

    return map[toString.call(obj)];
}


//颜色 rgb转16进制
function rgbTohex(color) {
    let rgb;

    if (color.indexOf("rgba") > -1) {
        rgb = color.replace("rgba(", "").replace(")", "").split(',');
    }
    else {
        rgb = color.replace("rgb(", "").replace(")", "").split(',');
    }

    let r = parseInt(rgb[0]);
    let g = parseInt(rgb[1]);
    let b = parseInt(rgb[2]);

    let hex = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);

    return hex;
};

//列下标  字母转数字
function ABCatNum(a) {

    // return ret;
    if(a==null || a.length==0){
        return NaN;
    }
    var str=a.toLowerCase().split("");
    var num=0;
    var al = str.length;
    var getCharNumber = function(charx){
        return charx.charCodeAt() -96;
    };
    var numout = 0;
    var charnum = 0;
    for(var i = 0; i < al; i++){
        charnum = getCharNumber(str[i]);
        numout += charnum * Math.pow(26, al-i-1);
    };
    // console.log(a, numout-1);
    if(numout==0){
        return NaN;
    }
    return numout-1;
};

//列下标  数字转字母
function chatatABC(n) {
    var orda = 'a'.charCodeAt(0); 
    var ordz = 'z'.charCodeAt(0); 
    var len = ordz - orda + 1; 
    var s = ""; 
   
    while( n >= 0 ) { 
   
        s = String.fromCharCode(n % len + orda) + s; 
   
        n = Math.floor(n / len) - 1; 
   
    } 
   
    return s.toUpperCase(); 
};

function showrightclickmenu($menu, x, y) {
    let winH = $(window).height(), winW = $(window).width();
    let menuW = $menu.width(), menuH = $menu.height();
    let top = y, left = x;

    if (x + menuW > winW) {
        left = x - menuW;
    }

    if (y + menuH > winH) {
        top = y - menuH;
    }

    if (top < 0) {
        top = 0;
    }

    $menu.css({ "top": top, "left": left }).show();
}