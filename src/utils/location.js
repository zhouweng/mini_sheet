
import Store from '../store';
import { luckysheet_searcharray } from '../utils/sheetSearch';

export {
    getMouseClickedPos,
    colLocationByIndex,
    rowLocationByIndex,
    mouseposition,
    colLocation,
    rowLocation,
}

function getMouseClickedPos(event) {
    let mouse = mouseposition(event.pageX, event.pageY);
    if (mouse[0] >= Store.cellmainWidth - Store.cellMainSrollBarSize || mouse[1] >= Store.cellmainHeight - Store.cellMainSrollBarSize) {
        return;
    }

    let x = mouse[0] + $("#luckysheet-cell-main").scrollLeft();
    let y = mouse[1] + $("#luckysheet-cell-main").scrollTop();
    
    let row_location = rowLocation(y);
    let col_location = colLocation(x);
        
    return [row_location, col_location];
}

function mouseposition(x, y) {
    let container_offset = $("#" + Store.container).offset();
    
    let newX = x - container_offset.left - Store.rowHeaderWidth,
        newY = y - container_offset.top  - Store.columnHeaderHeight - Store.toolbarHeight;  //- Store.infobarHeight  - Store.calculatebarHeight

    return [newX, newY];
}

function colLocation(x) {
    let col_index = luckysheet_searcharray(Store.visibledatacolumn, x);

    if (col_index == -1 && x > 0) {
        col_index = Store.visibledatacolumn.length - 1;
    }
    else if (col_index == -1 && x <= 0) {
        col_index = 0;
    }

    return colLocationByIndex(col_index);
}

function rowLocation(y) {
    let row_index = luckysheet_searcharray(Store.visibledatarow, y);

    if (row_index == -1 && y > 0) {
        row_index = Store.visibledatarow.length - 1;
    }
    else if (row_index == -1 && y <= 0) {
        row_index = 0;
    }
    
    return rowLocationByIndex(row_index);
}


function colLocationByIndex(col_index){
    let col = 0, col_pre = 0;            
    col = Store.visibledatacolumn[col_index];

    if (col_index == 0) {
        col_pre = 0;
    }
    else {
        col_pre = Store.visibledatacolumn[col_index - 1];
    }

    return [col_pre, col, col_index];
}

function rowLocationByIndex(row_index) {
    let row = 0, row_pre = 0;
    row = Store.visibledatarow[row_index];

    if (row_index == 0) {
        row_pre = 0;
    }
    else {
        row_pre = Store.visibledatarow[row_index - 1];
    }

    return [row_pre, row, row_index];
}
