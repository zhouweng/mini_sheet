export {
    gridHTML,
    luckysheetloadingHTML,
    luckysheetdefaultstyle,
    inputHTML,
    rightclickHTML,
    menuToolBar,
    flow,
    keycode,
    dropListMenu,
    iconfontObjects,
    colorList,
    colorItemList,
}

//dom variable
const gridHTML = function () {

    return `<div class="luckysheet">
                    <div class="luckysheet-work-area luckysheet-noselected-text"> 
                        <div id="luckysheet-wa-editor" class="luckysheet-wa-editor toolbar"> \${menu} </div> 
                    </div> 
                    <div class="luckysheet-grid-container luckysheet-scrollbars-enabled"> 
                        <div class="luckysheet-grid-window"> 
                            <div class="luckysheet-grid-window-1" id="luckysheet-grid-window-1">
                                <canvas id="luckysheetTableContent" class="luckysheetTableContent"></canvas> 
                                <table class="luckysheet-grid-window-2" cellspacing="0" cellpadding="0" dir="ltr" tabindex="-1" > 
                                    <tbody> 
                                        <tr> 
                                            <td valign="top" class="luckysheet-paneswrapper"> 
                                                <div class="luckysheet-left-top" id="luckysheet-left-top"> </div> 
                                            </td> 
                                            <td valign="top" class="luckysheet-paneswrapper"> 
                                                <div id="luckysheet-cols-h-c" class="luckysheet-cols-h-c">
                                                    <div class="luckysheet-cols-change-size" id="luckysheet-cols-change-size"></div>  
                                                    <div class="luckysheet-cols-h-hover" id="luckysheet-cols-h-hover"></div>  
                                                    <div id="luckysheet-cols-h-selected"></div>  
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="luckysheet-paneswrapper"> 
                                                <div class="luckysheet-rows-h" id="luckysheet-rows-h"> 
                                                    <div class="luckysheet-rows-change-size" id="luckysheet-rows-change-size"></div> 
                                                    <div class="luckysheet-rows-h-hover" id="luckysheet-rows-h-hover"></div> 
                                                    <div id="luckysheet-rows-h-selected"></div>  
                                                </div> 
                                            </td>  
                                            <td valign="top" class="luckysheet-paneswrapper">
                                                <div class="luckysheet-cell-loading" id="luckysheet-cell-loading"></div> 
                                                <div class="luckysheet-scrollbars luckysheet-scrollbar-ltr luckysheet-scrollbar-x" id="luckysheet-scrollbar-x"><div></div></div> 
                                                <div class="luckysheet-scrollbars luckysheet-scrollbar-ltr luckysheet-scrollbar-y" id="luckysheet-scrollbar-y"><div></div></div> 
                                                <div class="luckysheet-cell-main " id="luckysheet-cell-main">
                                                    <div id="luckysheet-selection-copy"></div>  
                                                    <div class="luckysheet-change-size-line" id="luckysheet-change-size-line"></div>  
                                                    <div id="luckysheet-cell-selected-boxs">
                                                        <div id="luckysheet-cell-selected" class="luckysheet-cell-selected">
                                                            <div class="luckysheet-cs-inner-border"></div>
                                                            <div class="luckysheet-cs-fillhandle"></div>
                                                        </div>
                                                    </div>
                                                    \${flow}
                                                </div> 
                                            </td> 
                                        </tr> 
                                    </tbody> 
                                </table> 
                            </div> 
                    </div>
                    <div id="luckysheet-copy-content" contenteditable="true"></div>
                  </div>`;
}

const luckysheetloadingHTML = function () {
    return '<div id="luckysheetloadingdata" style="width:100%;text-align:center;position:absolute;top:0px;height:100%;font-size: 16px;z-index:1000000000;background:#fff;"><div style="position:relative;top:45%;width:100%;"><div class="luckysheetLoaderGif"></div><span>Loading...</span></div></div>';
}

const inputHTML = '<div dir="ltr"><div class="luckysheet-input-box-index" id="luckysheet-input-box-index"></div><div id="luckysheet-input-box" spellcheck="false" aria-hidden="false" class="luckysheet-input-box"><div class="luckysheet-cell-input editable" tabindex="0" role="combobox" contenteditable="true" id="luckysheet-rich-text-editor" dir="ltr" g_editable="true" aria-autocomplete="list"></div></div></div>'
, flow = '<div id="luckysheet-cell-flow_${index}" class="luckysheet-cell-flow luckysheetsheetchange" style="width:${width}px;"><div class="luckysheet-cell-flow-clip">${flow}</div></div>'
, dropListMenu = '<div class="luckysheet-cols-menu luckysheet-rightgclick-menu luckysheet-menuButton ${subclass} luckysheet-mousedown-cancel" id="luckysheet-icon-${id}-menuButton">${item}</div>'
, colorList = '<div class="luckysheet-cols-menu luckysheet-rightgclick-menu luckysheet-rightgclick-menu-sub luckysheet-mousedown-cancel luckysheet-menuButton ${sub}" id="${id}"><div class="luckysheet-cols-menuitem luckysheet-mousedown-cancel luckysheet-color-reset"><div class="luckysheet-cols-menuitem-content luckysheet-mousedown-cancel">${resetColor}</div></div> <div class="luckysheet-mousedown-cancel"> <div class="luckysheet-mousedown-cancel"> <input type="text" class="luckysheet-color-selected" /> </div> </div> <div class="luckysheet-menuseparator luckysheet-mousedown-cancel" role="separator"></div> ${coloritem}</div>'
, colorItemList = '<div class="luckysheet-cols-menuitem luckysheet-mousedown-cancel ${class}"></div>'
;   
//右键菜单dom
function rightclickHTML(){

    const rightclickContainer =  `<div id="luckysheet-rightclick-menu" class="luckysheet-cols-menu luckysheet-rightgclick-menu luckysheet-mousedown-cancel">
                <div id="luckysheet-copy-btn" class="luckysheet-cols-menuitem luckysheet-mousedown-cancel luckysheet-copy-btn" data-clipboard-action="copy" data-clipboard-target="#luckysheet-copy-content" style="display:'block'};">
                    <div class="luckysheet-cols-menuitem-content luckysheet-mousedown-cancel">copy</div>
                </div>
                <div id="luckysheet-copy-paste" class="luckysheet-cols-menuitem luckysheet-mousedown-cancel" style="display:'block'};">
                    <div class="luckysheet-cols-menuitem-content luckysheet-mousedown-cancel">paste</div>
                </div>
                </div>
                `;
    return rightclickContainer;
}

// toolbar
function menuToolBar (){
    return `<div class="luckysheet-toolbar-left-theme">
     </div>
     
     <div id="toolbar-separator-font-family" class="luckysheet-toolbar-separator luckysheet-inline-block" style="user-select: none;">
     </div>
     <div class="luckysheet-toolbar-select luckysheet-toolbar-zoom-combobox luckysheet-toolbar-combo-button luckysheet-inline-block"
     data-tips="fontSize" id="luckysheet-icon-font-size" style="user-select: none;">
         <div class="luckysheet-toolbar-combo-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-combo-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div aria-posinset="4" aria-setsize="7" class="luckysheet-inline-block luckysheet-toolbar-combo-button-caption"
                 style="user-select: none;">
                     <input aria-label="fontSize" class="luckysheet-toolbar-combo-button-input luckysheet-toolbar-textinput"
                     role="combobox" style="user-select: none;" tabindex="-1" type="text" value="10"
                     />
                 </div>
                 <div class="luckysheet-toolbar-combo-button-dropdown luckysheet-inline-block iconfont luckysheet-iconfont-xiayige"
                 style="user-select: none;">
                 </div>
             </div>
         </div>
     </div>
     <div id="toolbar-separator-font-size" class="luckysheet-toolbar-separator luckysheet-inline-block" style="user-select: none;">
     </div>
     <div class="luckysheet-toolbar-button luckysheet-inline-block" 
     id="luckysheet-icon-bold" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-icon luckysheet-inline-block " style="user-select: none;">
                     <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-bold iconfont luckysheet-iconfont-jiacu"
                     style="user-select: none;">
                     </div>
                 </div>
             </div>
         </div>
     </div>
     
     <div class="luckysheet-toolbar-button luckysheet-inline-block" data-tips="italic"
     id="luckysheet-icon-italic" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-icon luckysheet-inline-block " style="user-select: none;">
                     <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-italic iconfont luckysheet-iconfont-wenbenqingxie1"
                     style="user-select: none;">
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="luckysheet-toolbar-button luckysheet-inline-block" data-tips="strikethrough"
     id="luckysheet-icon-strikethrough" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-icon luckysheet-inline-block " style="user-select: none;">
                     <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-strikethrough iconfont luckysheet-iconfont-wenbenshanchuxian"
                     style="user-select: none;">
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="luckysheet-toolbar-button luckysheet-inline-block" data-tips="underline"
     id="luckysheet-icon-underline" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-icon luckysheet-inline-block " style="user-select: none;">
                     <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-underline iconfont luckysheet-iconfont-wenbenxiahuaxian"
                     style="user-select: none;">
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div id="toolbar-separator-merge-cell" class="luckysheet-toolbar-separator luckysheet-inline-block" style="user-select: none;">
     </div>
     <div class="luckysheet-toolbar-button-split-left luckysheet-toolbar-button luckysheet-inline-block luckysheet-icon-merge-button"
      id="luckysheet-icon-merge-button" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-icon luckysheet-inline-block " style="user-select: none;">
                     <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-merge iconfont luckysheet-iconfont-hebing"
                     style="user-select: none;">
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="luckysheet-toolbar-button-split-right luckysheet-toolbar-menu-button luckysheet-inline-block"
     id="luckysheet-icon-merge-menu" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-menu-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-menu-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-toolbar-menu-button-dropdown luckysheet-inline-block iconfont luckysheet-iconfont-xiayige"
                 style="user-select: none;">
                 </div>
             </div>
         </div>
     </div>
     
     <div class="luckysheet-toolbar-button-split-left luckysheet-toolbar-button luckysheet-inline-block luckysheet-icon-cell-color"
      id="luckysheet-icon-cell-color" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-menu-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-toolbar-menu-button-caption luckysheet-inline-block"
                 style="user-select: none;">
                     <div class="luckysheet-color-menu-button-indicator" style="border-bottom-color: rgb(255, 255, 255); user-select: none;">
                         <div class="luckysheet-icon luckysheet-inline-block " style="user-select: none;">
                             <div class="text-color-bar" style="background-color: #fff "></div>
                             <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-cell-color iconfont luckysheet-iconfont-tianchong"
                             style="user-select: none;">
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="luckysheet-toolbar-button-split-right luckysheet-toolbar-menu-button luckysheet-inline-block"
     id="luckysheet-icon-cell-color-menu" role="button"
     style="user-select: none;">
         <div class="luckysheet-toolbar-menu-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-menu-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-toolbar-menu-button-dropdown luckysheet-inline-block iconfont luckysheet-iconfont-xiayige"
                 style="user-select: none;">
                 </div>
             </div>
         </div>
     </div>
     <div id="toolbar-separator-merge-cell" class="luckysheet-toolbar-separator luckysheet-inline-block" style="user-select: none;">
     </div>
     <div class="luckysheet-toolbar-button-split-left luckysheet-toolbar-button luckysheet-inline-block luckysheet-icon-align"
     id="luckysheet-icon-align" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-menu-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-toolbar-menu-button-caption luckysheet-inline-block"
                 style="user-select: none;">
                     <div class="luckysheet-icon luckysheet-inline-block " style="user-select: none;">
                         <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-align-left iconfont luckysheet-iconfont-wenbenzuoduiqi"
                         style="user-select: none;">
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="luckysheet-toolbar-button-split-right luckysheet-toolbar-menu-button luckysheet-inline-block"
     id="luckysheet-icon-align-menu" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-menu-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-menu-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-toolbar-menu-button-dropdown luckysheet-inline-block iconfont luckysheet-iconfont-xiayige"
                 style="user-select: none;">
                 </div>
             </div>
         </div>
     </div>
     <div class="luckysheet-toolbar-button-split-left luckysheet-toolbar-button luckysheet-inline-block luckysheet-icon-valign"
     id="luckysheet-icon-valign" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-menu-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-toolbar-menu-button-caption luckysheet-inline-block"
                 style="user-select: none;">
                     <div class="luckysheet-icon luckysheet-inline-block " style="user-select: none;">
                         <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-valign-bottom iconfont luckysheet-iconfont-dibuduiqi"
                         style="user-select: none;">
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="luckysheet-toolbar-button-split-right luckysheet-toolbar-menu-button luckysheet-inline-block"
     id="luckysheet-icon-valign-menu" role="button" style="user-select: none;">
         <div class="luckysheet-toolbar-menu-button-outer-box luckysheet-inline-block"
         style="user-select: none;">
             <div class="luckysheet-toolbar-menu-button-inner-box luckysheet-inline-block"
             style="user-select: none;">
                 <div class="luckysheet-toolbar-menu-button-dropdown luckysheet-inline-block iconfont luckysheet-iconfont-xiayige"
                 style="user-select: none;">
                 </div>
             </div>
         </div>
     </div>
     `;

     
}

const luckysheetdefaultstyle = {
    fillStyle: "#000000",
    textBaseline: "middle",
    strokeStyle: "#dfdfdf",
    rowFillStyle: "#5e5e5e",
    textAlign: 'center'
}


const keycode = {

    BACKSPACE: 8,
    TAB: 9,
    ENTER: 13,
    SHIFT: 16,
    CTRL: 17,
    PAUSE: 19,
    CAPSLOCK: 20,
    ESC: 27,

    SPACE: 33,
    PAGEUP: 33,
    PAGEDOWN: 34,
    END: 35,
    HOME: 36,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    INSERT: 45,
    DELETE: 46,

    WIN: 91,
    WIN_R: 92,
    MENU: 93,

    F1: 112,
    F2: 113,
    F3: 114,
    F4: 115,
    F5: 116,
    F6: 117,
    F7: 118,
    F8: 119,
    F9: 120,
    F10: 121,
    F11: 122,
    F12: 123,
    NUMLOCK: 144,
    SCROLLLOCK: 145
};


const iconfontObjects = {
    border:{
        'border-top': ' iconfont luckysheet-iconfont-shangbiankuang',
        'border-bottom': ' iconfont luckysheet-iconfont-xiabiankuang',
        'border-left': ' iconfont luckysheet-iconfont-zuobiankuang',
        'border-right': ' iconfont luckysheet-iconfont-youbiankuang',
        'border-none': ' iconfont luckysheet-iconfont-wubiankuang',
        'border-all': ' iconfont luckysheet-iconfont-quanjiabiankuang',
        'border-outside': ' iconfont luckysheet-iconfont-sizhoujiabiankuang',
        'border-inside': ' iconfont luckysheet-iconfont-neikuangxian',
        'border-horizontal': ' iconfont luckysheet-iconfont-neikuanghengxian',
        'border-vertical': ' iconfont luckysheet-iconfont-neikuangshuxian',
    },
    merge:{
        'all': ' iconfont luckysheet-iconfont-hebing',
        'cancel': ' iconfont luckysheet-iconfont-quxiaohebing',
    },
    align:{
        'left': ' iconfont luckysheet-iconfont-wenbenzuoduiqi',
        'center': ' iconfont luckysheet-iconfont-wenbenjuzhongduiqi',
        'right': ' iconfont luckysheet-iconfont-wenbenyouduiqi',
        'top': ' iconfont luckysheet-iconfont-dingbuduiqi',
        'middle': ' iconfont luckysheet-iconfont-shuipingduiqi',
        'bottom': ' iconfont luckysheet-iconfont-dibuduiqi',
    },
    textWrap:{
        'overflow': ' iconfont luckysheet-iconfont-yichu1',
        'wrap': ' iconfont luckysheet-iconfont-zidonghuanhang',
        'clip': ' iconfont luckysheet-iconfont-jieduan',
    },
    rotation:{
        'none': ' iconfont luckysheet-iconfont-wuxuanzhuang',
        'angleup': ' iconfont luckysheet-iconfont-xiangshangqingxie',
        'angledown': ' iconfont luckysheet-iconfont-xiangxiaqingxie',
        'vertical': ' iconfont luckysheet-iconfont-shupaiwenzi',
        'rotation-up': ' iconfont luckysheet-iconfont-wenbenxiangshang',
        'rotation-down': ' iconfont luckysheet-iconfont-xiangxia90',
    }
}