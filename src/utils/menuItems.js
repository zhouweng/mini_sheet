export {
    fontSizeItems,
    mergeItems,
    alignItems,
    valignItems,
}

let valignItems = [{
                "text": "top",
                "value": "top",
                "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-valign-top iconfont luckysheet-iconfont-dingbuduiqi" style="user-select: none;"> </div> </div>'
            },
            {
                "text": "middle",
                "value": "middle",
                "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-valign-middle iconfont luckysheet-iconfont-shuipingduiqi" style="user-select: none;"> </div> </div>'
            },
            {
                "text": "bottom",
                "value": "bottom",
                "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-valign-bottom iconfont luckysheet-iconfont-dibuduiqi" style="user-select: none;"> </div> </div>'
            }
        ];

let alignItems = [{
    "text": "left",
    "value": "left",
    "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-align-left iconfont luckysheet-iconfont-wenbenzuoduiqi" style="user-select: none;"> </div> </div>'
},
{
    "text": "center",
    "value": "center",
    "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-align-center iconfont luckysheet-iconfont-wenbenjuzhongduiqi" style="user-select: none;"> </div> </div>'
},
{
    "text": "right",
    "value": "right",
    "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-align-right iconfont luckysheet-iconfont-wenbenyouduiqi" style="user-select: none;"> </div> </div>'
}
];

let fontSizeItems = [{
    "text": "9",
    "value": "9",
    "example": ""
},
{
    "text": "10",
    "value": "10",
    "example": ""
},
{
    "text": "11",
    "value": "11",
    "example": ""
},
{
    "text": "12",
    "value": "12",
    "example": ""
},
{
    "text": "14",
    "value": "14",
    "example": ""
},
{
    "text": "16",
    "value": "16",
    "example": ""
},
{
    "text": "18",
    "value": "18",
    "example": ""
},
{
    "text": "20",
    "value": "20",
    "example": ""
},
{
    "text": "22",
    "value": "22",
    "example": ""
},
{
    "text": "24",
    "value": "24",
    "example": ""
},
{
    "text": "26",
    "value": "26",
    "example": ""
},
{
    "text": "28",
    "value": "28",
    "example": ""
},
{
    "text": "36",
    "value": "36",
    "example": ""
},
{
    "text": "48",
    "value": "48",
    "example": ""
},
{
    "text": "72",
    "value": "72",
    "example": ""
}
];

let mergeItems = [{
    "text": "Merge All",
    "value": "mergeAll",
    "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-align-left iconfont luckysheet-iconfont-hebing" style="user-select: none;"> </div> </div>'
},
{
    "text": "Vertically",
    "value": "mergeV",
    "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-align-left iconfont luckysheet-iconfont-chuizhihebing" style="user-select: none;"> </div> </div>'
},
{
    "text": "Horizontally",
    "value": "mergeH",
    "example": '<div class="luckysheet-icon luckysheet-inline-block" style="user-select: none;opacity:1;"> <div aria-hidden="true" class="luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-align-left iconfont luckysheet-iconfont-shuipinghebing" style="user-select: none;"> </div> </div>'
}
];