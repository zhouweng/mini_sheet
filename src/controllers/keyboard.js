

import {
    copy,
} from './selection';
import {
    selectHightlightShow
} from '../view/select';
import {
    cleargridelement
} from '../view/cleargridelement';
import Store from '../store';
import {
    keycode
} from '../utils/constant';


export function keyboardInitial() {

    //单元格编辑输入
    $("#luckysheet-input-box").click(function () {
        // formula.rangeHightlightselected($("#luckysheet-rich-text-editor"));
    }).add("#" + Store.container).on("keydown", function (event) {
        let ctrlKey = event.ctrlKey;
        let kcode = event.keyCode;

        if (ctrlKey || event.metaKey) {
            if (kcode == 67) { //Ctrl + C  复制
                if (Store.luckysheet_select_save.length == 0) {
                    return;
                }
                copy(event);
                event.stopPropagation();

                return;
            } else if (kcode == 86) { //Ctrl + V  粘贴
                if (Store.luckysheet_select_save.length > 1) {
                    alert("please choose one cell");
                    return;
                }
                event.stopPropagation();
                return;
            } else if (kcode == 85) {//Ctrl + U  下划线
                $("#luckysheet-icon-underline").click();
            }
            else if (kcode == 73) {//Ctrl + I  斜体
                $("#luckysheet-icon-italic").click();
            }
            else if (kcode == 66) {//Ctrl + B  加粗
                $("#luckysheet-icon-bold").click();
            }
            event.preventDefault();
            return;
        } else if (kcode == keycode.ESC) {
            cleargridelement(event);
            event.preventDefault();

            selectHightlightShow();
        }


        event.stopPropagation();
    });


}