import {
    mouseposition,
    colLocation,
    rowLocation,
} from '../utils/location';
import {
    luckysheetrefreshgrid
} from '../view/refresh';
import {
    rhchInit
} from '../model/rhchInit';
import Store from '../store';

export {
    rowColumnOperationInitial,
}


function rowColumnOperationInitial() {

    //表格行标题 mouse事件
    $("#luckysheet-rows-h").mousedown(function (event) {

    }).mousemove(function (event) {
        if (Store.luckysheet_rows_change_size) {
            let mouse = mouseposition(event.pageX, event.pageY);
            let scrollTop = $("#luckysheet-rows-h").scrollTop();
            let y = mouse[1] + scrollTop;
            let winH = $(window).height();

            if ((y + 3) - Store.luckysheet_rows_change_size_start[0] > 19 && y < winH + scrollTop - 200) {
                $("#luckysheet-change-size-line").css({ "top": y });
                $("#luckysheet-rows-change-size").css({ "top": y });
            }
        } else {
            if (Store.luckysheet_rows_selected_status || Store.luckysheet_select_status) {
                $("#luckysheet-rows-h-hover").hide();
                return;
            }
    
            let mouse = mouseposition(event.pageX, event.pageY);
            let y = mouse[1] + $("#luckysheet-rows-h").scrollTop();
    
            let row_location = rowLocation(y), 
                row = row_location[1], 
                row_pre = row_location[0];
    
            $("#luckysheet-rows-h-hover").css({ "top": row_pre, "height": row - row_pre - 1, "display": "block" });
    
            if (y < row - 1 && y >= row - 5) {
                $("#luckysheet-rows-change-size").css({ "top": row - 3, "opacity": 0 });
            }
            else {
                $("#luckysheet-rows-change-size").css("opacity", 0);
            }

        }
    }).mouseleave(function (event) {
        $("#luckysheet-rows-h-hover").hide();
        $("#luckysheet-rows-change-size").css("opacity", 0);
    }).mouseup(function (event) {
        
        //改变行高
        if (Store.luckysheet_rows_change_size) {
            Store.luckysheet_rows_change_size = false;

            $("#luckysheet-change-size-line").hide();
            $("#luckysheet-rows-change-size").css("opacity", 0);
            $("#luckysheet-sheettable, #luckysheet-rows-h, #luckysheet-rows-h canvas").css("cursor", "default");

            let mouse = mouseposition(event.pageX, event.pageY);
            let scrollTop = $("#luckysheet-rows-h").scrollTop();
            let y = mouse[1] + scrollTop;
            let winH = $(window).height();

            let size = (y + 3) - Store.luckysheet_rows_change_size_start[0];

            if ((y + 3) - Store.luckysheet_rows_change_size_start[0] < 19) {
                size = 19;
            }

            if (y >= winH - 200 + scrollTop) {
                size = winH - 200 - Store.luckysheet_rows_change_size_start[0] + scrollTop;
            }

            let cfg = $.extend(true, {}, Store.config);
            if (cfg["rowlen"] == null) {
                cfg["rowlen"] = {};
            }

            if (cfg["customHeight"] == null) {
                cfg["customHeight"] = {};
            }

            cfg["customHeight"][Store.luckysheet_rows_change_size_start[1]] = 1;

            const changeRowIndex = Store.luckysheet_rows_change_size_start[1];
            let changeRowSelected = false;
            if(Store["luckysheet_select_save"].length > 0) {
                Store["luckysheet_select_save"].filter(select => select.row_select).some((select) => {
                    if(changeRowIndex >= select.row[0] && changeRowIndex <= select.row[1]) {
                        changeRowSelected = true;
                    }
                    return changeRowSelected;
                });
            }
            if(changeRowSelected) {
                Store["luckysheet_select_save"].filter(select => select.row_select).forEach(select => {
                    for(let r = select.row[0]; r <= select.row[1]; r++) {
                        cfg["rowlen"][r] = Math.ceil(size/Store.zoomRatio);  
                    }
                })
            } 
            else {
                cfg["rowlen"][Store.luckysheet_rows_change_size_start[1]] = Math.ceil(size/Store.zoomRatio);
            }


            //config
            Store.config = cfg;

            // jfrefreshgrid_rhcw(Store.flowdata.length, null);
            rhchInit(Store.flowdata.length, null); //From: refresh.jfrefreshgrid_rhcw()
            luckysheetrefreshgrid();  
        }
    });


    //表格行标题 改变行高按钮
    $("#luckysheet-rows-change-size").mousedown(function (event) {
        
        $("#luckysheet-input-box").hide();
        $("#luckysheet-rows-change-size").css({ "opacity": 1 });

        let mouse = mouseposition(event.pageX, event.pageY);
        let y = mouse[1] + $("#luckysheet-rows-h").scrollTop();

        let scrollLeft = $("#luckysheet-cell-main").scrollLeft();
        let winW = $("#luckysheet-cell-main").width();

        let row_location = rowLocation(y), 
            row = row_location[1], 
            row_pre = row_location[0], 
            row_index = row_location[2];

        Store.luckysheet_rows_change_size = true;
        Store.luckysheet_scroll_status = true;
        $("#luckysheet-change-size-line").css({ 
            "height": "1px", 
            "border-width": 
            "0 0px 1px 0", 
            "top": row - 3, 
            "left": 0, 
            "width": scrollLeft + winW, 
            "display": "block", 
            "cursor": "ns-resize" 
        });
        $("#luckysheet-sheettable, #luckysheet-rows-h, #luckysheet-rows-h canvas").css("cursor", "ns-resize");
        Store.luckysheet_rows_change_size_start = [row_pre, row_index];
        $("#luckysheet-rightclick-menu").hide();
        $("#luckysheet-rows-h-hover").hide();
        $("#luckysheet-cols-menu-btn").hide();
        event.stopPropagation();
    });

    //表格列标题 mouse事件
    $("#luckysheet-cols-h-c").mousedown(function (event) {

    }).mousemove(function (event) { // highlight change-size-line
        if (Store.luckysheet_cols_change_size) {
            let mouse = mouseposition(event.pageX, event.pageY);
            let scrollLeft = $("#luckysheet-cols-h-c").scrollLeft();
            let x = mouse[0] + scrollLeft;
            let winW = $(window).width();
            
            if ((x + 3) - Store.luckysheet_cols_change_size_start[0] > 30 && x < winW + scrollLeft - 100) {
                $("#luckysheet-change-size-line").css({ "left": x });
                $("#luckysheet-cols-change-size").css({ "left": x - 2 });
            }
        } else {

            let mouse = mouseposition(event.pageX, event.pageY);
            let x = mouse[0] + $("#luckysheet-cols-h-c").scrollLeft();
    
            let col_location = colLocation(x),
                col = col_location[1],
                col_pre = col_location[0];
    
            $("#luckysheet-cols-h-hover").css({
                "left": col_pre,
                "width": col - col_pre - 1,
                "display": "block"
            });
            $("#luckysheet-cols-change-size").css({
                "left": col - 5
            });
            if (x < col && x >= col - 5) {
                $("#luckysheet-cols-change-size").css({
                    "opacity": 0
                });
            } else {
                $("#luckysheet-change-size-line").hide();
                $("#luckysheet-cols-change-size").css("opacity", 0);
            }
        }
    })
    .mouseleave(function (event) {
        if (Store.luckysheet_cols_change_size) {
            return;
        }

        $("#luckysheet-cols-h-hover").hide();
        $("#luckysheet-cols-change-size").css("opacity", 0);
    })
    .mouseup(function (event) {
        //From: handler.js if (Store.luckysheet_cols_change_size) {}
        //改变列宽
        if (Store.luckysheet_cols_change_size) {
            Store.luckysheet_cols_change_size = false;
            $("#luckysheet-change-size-line").hide();
            $("#luckysheet-cols-change-size").css("opacity", 0);
            $("#luckysheet-sheettable, #luckysheet-cols-h-c, .luckysheet-cols-h-cells, .luckysheet-cols-h-cells canvas").css("cursor", "default");
            
            let mouse = mouseposition(event.pageX, event.pageY);
            let scrollLeft = $("#luckysheet-cols-h-c").scrollLeft();
            let x = mouse[0] + scrollLeft;
            let winW = $(window).width();

            let size = (x + 3) - Store.luckysheet_cols_change_size_start[0];

            
            let firstcolumnlen = Store.defaultcollen;
            if (Store.config["columnlen"] != null && Store.config["columnlen"][Store.luckysheet_cols_change_size_start[1]] != null) {
                firstcolumnlen = Store.config["columnlen"][Store.luckysheet_cols_change_size_start[1]];
            }

            if (Math.abs(size - firstcolumnlen) < 3) {
                return;
            }
            if ((x + 3) - Store.luckysheet_cols_change_size_start[0] < 30) {
                size = 30;
            }

            if (x >= winW - 100 + scrollLeft) {
                size = winW - 100 - Store.luckysheet_cols_change_size_start[0] + scrollLeft;
            }

            let cfg = $.extend(true, {}, Store.config);
            if (cfg["columnlen"] == null) {
                cfg["columnlen"] = {};
            }

            if (cfg["customWidth"] == null) {
                cfg["customWidth"] = {};
            }

            cfg["customWidth"][Store.luckysheet_cols_change_size_start[1]] = 1;

            const changeColumnIndex = Store.luckysheet_cols_change_size_start[1];
            let changeColumnSelected = false;
            if(Store["luckysheet_select_save"].length > 0) {
                Store["luckysheet_select_save"].filter(select => select.column_select).some((select) => {
                    if(changeColumnIndex >= select.column[0] && changeColumnIndex <= select.column[1]) {
                        changeColumnSelected = true;
                    }
                    return changeColumnSelected;
                });
            }
            if(changeColumnSelected) {
                Store["luckysheet_select_save"].filter(select => select.column_select).forEach(select => {
                    for(let r = select.column[0]; r <= select.column[1]; r++) {
                        cfg["columnlen"][r] = Math.ceil(size/Store.zoomRatio);  
                    }
                })
            } 
            else {
                cfg["columnlen"][Store.luckysheet_cols_change_size_start[1]] = Math.ceil(size/Store.zoomRatio);
            }
            
            //config
            Store.config = cfg;
            rhchInit(null, Store.flowdata[0].length); //From: refresh.jfrefreshgrid_rhcw()
            luckysheetrefreshgrid();  
        }
    })

    //表格列标题 改变列宽按钮
    $("#luckysheet-cols-change-size").mousedown(function (event) {

        $("#luckysheet-input-box").hide();
        $("#luckysheet-cols-change-size").css({
            "opacity": 1
        });

        let mouse = mouseposition(event.pageX, event.pageY);
        let scrollLeft = $("#luckysheet-cols-h-c").scrollLeft();
        let scrollTop = $("#luckysheet-cell-main").scrollTop();
        let winH = $("#luckysheet-cell-main").height();
        let x = mouse[0] + scrollLeft;

        let col_location = colLocation(x),
            col = col_location[1],
            col_pre = col_location[0],
            col_index = col_location[2];

        Store.luckysheet_cols_change_size = true;
        $("#luckysheet-change-size-line").css({ // Draw line on cell main area
            "height": winH + scrollTop,
            "border-width": "0 1px 0 0",
            "top": 0,
            "left": col - 3,
            "width": "1px",
            "display": "block",
            "cursor": "ew-resize"
        });
        $("#luckysheet-sheettable, #luckysheet-cols-h-c, .luckysheet-cols-h-cells, .luckysheet-cols-h-cells canvas").css("cursor", "ew-resize");
        Store.luckysheet_cols_change_size_start = [col_pre, col_index];
        $("#luckysheet-cols-h-hover").hide();
        event.stopPropagation();
    });

}