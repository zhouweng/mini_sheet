
import {
    isRealNull
} from '../utils/validate';
import {
    getObjType,
} from '../utils/util';
import Store from '../store';

export {
    updateFormat_mc_cancel,
    updateFormat_mc,
    isMerged,
}

function isMerged(d) {
    let isHasMc = false;
    for (let i = 0; i < Store.luckysheet_select_save.length; i++) {
        let range = Store.luckysheet_select_save[i];
        let r1 = range["row"][0],
            r2 = range["row"][1];
        let c1 = range["column"][0],
            c2 = range["column"][1];

        for (let r = r1; r <= r2; r++) {
            for (let c = c1; c <= c2; c++) {
                let cell = d[r][c];

                if (getObjType(cell) == "object" && ("mc" in cell)) {
                    isHasMc = true;
                    break;
                }
                if (isHasMc == true) break;
            }
        }
    }

    return isHasMc;
}

function updateFormat_mc_cancel(d) {

    for (let i = 0; i < Store.luckysheet_select_save.length; i++) {
        let range = Store.luckysheet_select_save[i];
        let r1 = range["row"][0],
            r2 = range["row"][1];
        let c1 = range["column"][0],
            c2 = range["column"][1];

        if (r1 == r2 && c1 == c2) {
            continue;
        }

        let fv = {};

        for (let r = r1; r <= r2; r++) {
            for (let c = c1; c <= c2; c++) {
                let cell = d[r][c];

                if (cell != null && cell.mc != null) {
                    let mc_r = cell.mc.r,
                        mc_c = cell.mc.c;

                    if ("rs" in cell.mc) { // Top-left cell
                        delete cell.mc;
                        // delete cfg["merge"][mc_r + "_" + mc_c];

                        fv[mc_r + "_" + mc_c] = $.extend(true, {}, cell);
                    } else {
                        let cell_clone = fv[mc_r + "_" + mc_c];

                        delete cell_clone.v;
                        delete cell_clone.m;
                        delete cell_clone.ct;
                        delete cell_clone.f;
                        delete cell_clone.spl;

                        d[r][c] = cell_clone;
                    }
                }
            }
        }
    }
}

function updateFormat_mc(d, mergeType) {
    let cfg = $.extend(true, {}, Store.config);
    if (cfg["merge"] == null) {
        cfg["merge"] = {};
    }

    for (let i = 0; i < Store.luckysheet_select_save.length; i++) {
        let range = Store.luckysheet_select_save[i];
        let r1 = range["row"][0],
            r2 = range["row"][1];
        let c1 = range["column"][0],
            c2 = range["column"][1];

        if (r1 == r2 && c1 == c2) {
            continue;
        }

        if (mergeType == "mergeAll") {
            let fv = {},
                isfirst = false;

            for (let r = r1; r <= r2; r++) {
                for (let c = c1; c <= c2; c++) {
                    let cell = d[r][c];

                    if (cell != null && (!isRealNull(cell.v) || cell.f != null) && !isfirst) {
                        fv = $.extend(true, {}, cell);
                        isfirst = true;
                    }

                    d[r][c] = {
                        "mc": {
                            "r": r1,
                            "c": c1
                        }
                    };
                }
            }

            d[r1][c1] = fv;
            d[r1][c1].mc = {
                "r": r1,
                "c": c1,
                "rs": r2 - r1 + 1,
                "cs": c2 - c1 + 1
            };

            cfg["merge"][r1 + "_" + c1] = {
                "r": r1,
                "c": c1,
                "rs": r2 - r1 + 1,
                "cs": c2 - c1 + 1
            };
        } else if (mergeType == "mergeV") {
            for (let c = c1; c <= c2; c++) {
                let fv = {},
                    isfirst = false;

                for (let r = r1; r <= r2; r++) {
                    let cell = d[r][c];

                    if (cell != null && (!isRealNull(cell.v) || cell.f != null) && !isfirst) {
                        fv = $.extend(true, {}, cell);
                        isfirst = true;
                    }

                    d[r][c] = {
                        "mc": {
                            "r": r1,
                            "c": c
                        }
                    };
                }

                d[r1][c] = fv;
                d[r1][c].mc = {
                    "r": r1,
                    "c": c,
                    "rs": r2 - r1 + 1,
                    "cs": 1
                };

                cfg["merge"][r1 + "_" + c] = {
                    "r": r1,
                    "c": c,
                    "rs": r2 - r1 + 1,
                    "cs": 1
                };
            }
        } else if (mergeType == "mergeH") {
            for (let r = r1; r <= r2; r++) {
                let fv = {},
                    isfirst = false;

                for (let c = c1; c <= c2; c++) {
                    let cell = d[r][c];

                    if (cell != null && (!isRealNull(cell.v) || cell.f != null) && !isfirst) {
                        fv = $.extend(true, {}, cell);
                        isfirst = true;
                    }

                    d[r][c] = {
                        "mc": {
                            "r": r,
                            "c": c1
                        }
                    };
                }

                d[r][c1] = fv;
                d[r][c1].mc = {
                    "r": r,
                    "c": c1,
                    "rs": 1,
                    "cs": c2 - c1 + 1
                };

                cfg["merge"][r + "_" + c1] = {
                    "r": r,
                    "c": c1,
                    "rs": 1,
                    "cs": c2 - c1 + 1
                };
            }
        }
        Store.config = cfg;

    }
}
