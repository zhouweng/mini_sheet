
import Store from '../store';
import {
    getObjType,
} from '../utils/util';
export {
    updateFormat,
}

function updateFormat(d, attr, foucsStatus) {
    for (let s = 0; s < Store.luckysheet_select_save.length; s++) {
        let row_st = Store.luckysheet_select_save[s]["row"][0],
            row_ed = Store.luckysheet_select_save[s]["row"][1];
        let col_st = Store.luckysheet_select_save[s]["column"][0],
            col_ed = Store.luckysheet_select_save[s]["column"][1];

        updateFormatCell(d, attr, foucsStatus, row_st, row_ed, col_st, col_ed);
    }
}

//private function
function updateFormatCell(d, attr, foucsStatus, row_st, row_ed, col_st, col_ed) {
    if (d == null || attr == null) {
        return;
    }

    for (let r = row_st; r <= row_ed; r++) {
        if (Store.config["rowhidden"] != null && Store.config["rowhidden"][r] != null) {
            continue;
        }

        if (attr == "ht") {
            if (foucsStatus == "left") {
                foucsStatus = "1";
            } else if (foucsStatus == "center") {
                foucsStatus = "0";
            } else if (foucsStatus == "right") {
                foucsStatus = "2";
            }
        } else if (attr == "vt") {
            if (foucsStatus == "top") {
                foucsStatus = "1";
            } else if (foucsStatus == "middle") {
                foucsStatus = "0";
            } else if (foucsStatus == "bottom") {
                foucsStatus = "2";
            }
        }

        for (let c = col_st; c <= col_ed; c++) {
            let value = d[r][c];
            if (getObjType(value) == "object") {
                d[r][c][attr] = foucsStatus;
            } else { // process NULL cell 
                d[r][c] = {
                    v: value
                };
                d[r][c][attr] = foucsStatus;
            }
        }
    }
}
