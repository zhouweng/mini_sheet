
import {
    getObjType
} from '../utils/util';
import Store from '../store';

export {
    mergeborder
}


function mergeborder(flowdata, row_index, col_index){
    if(flowdata==null || flowdata[row_index]==null){
        console.warn("Merge info is null", row_index, col_index);
        return null;
    }
    let value = flowdata[row_index][col_index];
    
    if(getObjType(value) == "object" && ("mc" in value)){
        let margeMaindata = value["mc"];
        if(margeMaindata==null){
            console.warn("Merge info is null", row_index, col_index);
            return null;
        }
        col_index = margeMaindata.c;
        row_index = margeMaindata.r;


        if(flowdata[row_index][col_index]==null){
            console.warn("Main merge Cell info is null", row_index, col_index);
            return null;
        }
        let col_rs = flowdata[row_index][col_index].mc.cs;
        let row_rs = flowdata[row_index][col_index].mc.rs;

        let margeMain = flowdata[row_index][col_index].mc;
        
        let start_r, end_r, row, row_pre;
        for(let r = row_index; r < margeMain.rs + row_index; r++){
            if (r == 0) {
                start_r = - 1;
            }
            else {
                start_r = Store.visibledatarow[r - 1] - 1;
            }

            end_r = Store.visibledatarow[r];

            if(row_pre == null){
                row_pre = start_r;
                row = end_r;
            }
            else{
                row += end_r - start_r - 1;
            }
        }

        let start_c, end_c, col, col_pre; 
        for(let c = col_index; c < margeMain.cs + col_index; c++){
            if (c == 0) {
                start_c = 0;
            }
            else {
                start_c = Store.visibledatacolumn[c - 1];
            }

            end_c = Store.visibledatacolumn[c];

            if(col_pre == null){
                col_pre = start_c;
                col = end_c;
            }
            else{
                col += end_c - start_c;
            }
        }

        return {
            "row": [row_pre , row, row_index, row_index + row_rs - 1], 
            "column": [col_pre, col , col_index, col_index + col_rs - 1]
        };
    }
    else{
        return null;
    }
}
