import Store from '../store';
import { getObjType } from '../utils/util';
import {
    isRealNull
} from '../utils/validate';
import {
    luckysheetfontformat,
    rgbTohex,
} from '../utils/util';

export {
    getStatusByCell,
    getStatusByFlow,
    getCellTextInfo,
    getcellvalue,
    datagridgrowth,
    getRealCellValue,
}

function getStatusByCell(cell, type){
    let foucsStatus =cell;
    let tf = {"bl":1, "it":1 , "ff":1, "cl":1, "un":1};

    if(type in tf ){
        if(foucsStatus == null){
            foucsStatus = "0";
        }
        else{
            foucsStatus = foucsStatus[type];
            
            if(foucsStatus == null){
                foucsStatus = "0";
            }
        }
    }
    else if(type == "fc"){ // font color
        if(foucsStatus == null){
            foucsStatus = "#000000";
        }
        else{
            foucsStatus = foucsStatus[type];

            if(foucsStatus == null){
                foucsStatus = "#000000";
            }

            if(foucsStatus.indexOf("rgba") > -1){
                foucsStatus = rgbTohex(foucsStatus);
            }
        }
    }
    else if(type == "bg"){ // cell background
        if(foucsStatus == null){
            foucsStatus = null;
        }
        else{
            foucsStatus = foucsStatus[type];

            if(foucsStatus == null){
                foucsStatus = null;
            }
            else if(foucsStatus.toString().indexOf("rgba") > -1){
                foucsStatus = rgbTohex(foucsStatus);
            }
        }
    }
    else if(type == "fs"){
        if(foucsStatus == null){
            foucsStatus = "10";
        }
        else{
            foucsStatus = foucsStatus[type];
            if(foucsStatus == null){
                foucsStatus = "10";
            }
        }
    }
    else if(type == "ht"){
        if(foucsStatus == null){
            foucsStatus = "1";
        }
        else{
            foucsStatus = foucsStatus[type];
            if(foucsStatus == null){
                foucsStatus = "1";
            }
        }

        if(["0", "1", "2"].indexOf(foucsStatus.toString()) == -1){
            foucsStatus = "1";
        }
    }
    else if(type == "vt"){//默认垂直居中
        if(foucsStatus == null){
            foucsStatus = "0";
        }
        else{
            foucsStatus = foucsStatus[type];
            if(foucsStatus == null){
                foucsStatus = "0";
            }
        }

        if(["0", "1", "2"].indexOf(foucsStatus.toString()) == -1){
            foucsStatus = "0";
        }
    }
    else if(type == "mc"){
        if(foucsStatus == null){
            foucsStatus="0";
        } else {
            foucsStatus="1";

        }
    }

    return foucsStatus;
}


//From: ../controller/menuButton.checkstatus()
function getStatusByFlow(flowdata, row, column, type) {
    if (flowdata == null || flowdata[row] == null) {
        console.warn("It's incorrect data", row, column);
        return null;
    }
    let foucsStatus = flowdata[row][column];

    return getStatusByCell(foucsStatus, type);
}

//获取单元格文本内容的渲染信息
// From: luckysheet's getRowlen.js 
function getCellTextInfo(cell, ctx, option) {
    let cellWidth = option.cellWidth;
    let cellHeight = option.cellHeight;

    let space_width = option.space_width,
        space_height = option.space_height; //宽高方向 间隙

    if (space_width == null) {
        space_width = 2;
    }

    if (space_height == null) {
        space_height = 2;
    }

    let rt = 0;


    rt = parseInt(rt);
    ctx.textAlign = "start";

    let textContent = {};
    textContent.values = [];

    let fontset, cancelLine = "0",
        underLine = "0",
        fontSize = 11,
        value;

    fontset = luckysheetfontformat(cell);
    ctx.font = fontset;

    //水平对齐
    let horizonAlign = getStatusByCell(cell, "ht");
    //垂直对齐
    let verticalAlign = getStatusByCell(cell, "vt");
    cancelLine = getStatusByCell(cell, "cl"); //cancelLine
    underLine = getStatusByCell(cell, "un"); //underLine
    fontSize = getStatusByCell(cell, "fs");

    if (cell instanceof Object) {
        value = cell.m;
        if (value == null) {
            value = cell.v;
        }
    } else {
        value = cell;
    }

    if (isRealNull(value)) {
        return null;
    }

    ctx.textBaseline = 'alphabetic';

    let textWidth = ctx.measureText(value).width * Store.zoomRatio;
    let textActualBoundingBoxDescent = ctx.measureText(value).actualBoundingBoxDescent * Store.zoomRatio;
    let textActualBoundingBoxAscent = ctx.measureText(value).actualBoundingBoxAscent * Store.zoomRatio;
    let textHeight = textActualBoundingBoxDescent + textActualBoundingBoxAscent;

    let height = textHeight + textHeight / 2 - textActualBoundingBoxDescent - space_height;
    let left = space_width; //默认为1，左对齐
    if(horizonAlign == "0"){ //居中对齐
        left = cellWidth / 2  - (textWidth / 2) ;
    }
    else if(horizonAlign == "2"){ //右对齐
        left = (cellWidth - space_width)  - textWidth ;
    }
    let top = (cellHeight - space_height) - textHeight + textActualBoundingBoxAscent;//默认为2，下对齐
    if(verticalAlign == "0"){ //居中对齐
        top = cellHeight / 2  - (textHeight / 2) + textActualBoundingBoxAscent;
    }
    else if(verticalAlign == "1"){ //上对齐
        top = space_height + ctx.measureText(value).actualBoundingBoxAscent;
    }

    textContent.type = "plain";

    let wordGroup = {
        content: value,
        style: fontset,
        width: textWidth, // width,
        height: textHeight, // height,
        left:  left,
        top: top,
    }


    drawLineInfo(wordGroup, cancelLine, underLine, {
        width: textWidth,
        height: textHeight,
        left: left,
        top: top,
        asc: textActualBoundingBoxAscent, // measureText.actualBoundingBoxAscent,
        desc: textActualBoundingBoxDescent, // measureText.actualBoundingBoxDescent,
        fs: fontSize,
    });

    textContent.values.push(wordGroup);
    textContent.textLeftAll = left;
    textContent.textTopAll = top;
    textContent.asc = textActualBoundingBoxAscent;
    textContent.desc = textActualBoundingBoxDescent;


    return textContent;
}
//Get the value of the cell
function getcellvalue(r, c, data, type) {
    if (type == null) {
        type = "v";
    }

    if (data == null) {
        data = Store.flowdata;
    }

    let d_value;

    if (r != null && c != null) {
        d_value = data[r][c];
    }
    else if (r != null) {
        d_value = data[r];
    }
    else if (c != null) {
        let newData = data[0].map(function(col, i) {
            return data.map(function(row) {
                return row[i];
            })
        });
        d_value = newData[c];
    }
    else {
        return data;
    }

    let retv = d_value;

    if(getObjType(d_value) == "object"){
        retv = d_value[type];

        if (type == "f" && retv != null) {
            // retv = formula.functionHTMLGenerate(retv);
        }
        else if(type == "f") {
            retv = d_value["v"];
        }
        else if(d_value && d_value.ct && d_value.ct.t == 'd') {
            retv = d_value.m;
        }
    }

    if(retv == undefined){
        retv = null;
    }

    return retv;
}

//Data increase in rows and columns
function datagridgrowth(data, addr, addc) {
    if (addr <= 0 && addc <= 0) {
        return data;
    }

    if (addr <= 0) {
        addr = 0;
    }

    if (addc <= 0) {
        addc = 0;
    }

    let dataClen = 0;
    if (data.length == 0) {
        data = [];
        dataClen = 0;
    }
    else {
        dataClen = data[0].length;
    }

    let coladd = [];//需要额外增加的空列
    for (let c = 0; c < addc; c++) {
        coladd.push(null);
    }

    let rowadd = [];//完整的一个空行
    for (let r = 0; r < dataClen + addc; r++) {
        rowadd.push(null);
    }

    for (let r = 0; r < data.length; r++) {
        data[r] = [].concat(data[r].concat(coladd));
    }

    for (let r = 0; r < addr; r++) {
        data.push([].concat(rowadd));
    }


    return data;
}


function getRealCellValue(r, c){
    let value = getcellvalue(r, c, null, "m");
    if(value == null){
        value = getcellvalue(r, c);
    }

    return value;
}


function drawLineInfo(wordGroup, cancelLine, underLine, option) {
    let left = option.left,
        top = option.top,
        width = option.width,
        height = option.height,
        asc = option.asc,
        desc = option.desc,
        fs = option.fs;

    if (wordGroup.wrap === true) {
        return;
    }

    if (wordGroup.inline == true && wordGroup.style != null) {
        cancelLine = wordGroup.style.cl;
        underLine = wordGroup.style.un;
    }

    if (cancelLine != "0") {
        wordGroup.cancelLine = {};
        wordGroup.cancelLine.startX = left;
        wordGroup.cancelLine.startY = top - asc / 2 + 1;
        wordGroup.cancelLine.endX = left + width;
        wordGroup.cancelLine.endY = top - asc / 2 + 1;
        wordGroup.cancelLine.fs = fs;

    }

    if (underLine != "0") {
        wordGroup.underLine = [];
        let item = {};
        item.startX = left;
        item.startY = top;
        item.endX = left + width;
        item.endY = top;
        item.fs = fs;
        wordGroup.underLine.push(item);
    }
}