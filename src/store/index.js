export default Store;

const Store = {
    container: null,
    luckysheetfile: null,
    visibledatarow: [], 
    visibledatacolumn: [],
    luckysheetCellUpdate: null, //  cell[r,c] is in edit
    jfautoscrollTimeout: null, // mousemove event functio
    luckysheet_select_save: [{ "row": [0, 0], "column": [0, 0] }],  // cell area that be selected
    flowdata: [],  // cell data
    config: {},  // config of shell
    luckysheet_select_status: false,  // Has user select one cell?

    toolbarHeight: 40, // toolbar's height (px)
    defaultFontSize: 10,
    defaultcolumnNum: 60, // how many cells in each column
    defaultrowNum: 84,// how many cells in each row
    rowHeaderWidth: 46,  //Top first line row header width
    columnHeaderHeight: 30, //Left first line column header's height
    defaultcollen: 73, // The width of cell unit
    defaultrowlen: 19, // The height of cell unit
    ch_width: 0,   // scrollX's width
    rh_height: 0,  // scrollY's width
    cellMainSrollBarSize: 12,  // Scroll bar's width
    luckysheetTableContentHW: [0, 0],  // cell area + header - scroll width

    luckysheet_rows_change_size: false,   //row&column resize
    luckysheet_rows_change_size_start: [],
    luckysheet_cols_change_size: false,
    luckysheet_cols_change_size_start: [],
    
    zoomRatio: 1,
}
