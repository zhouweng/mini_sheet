
import Store from '../store';
import {
    colLocationByIndex,
    rowLocationByIndex,
} from '../utils/location';

export {
    selectHightlightShow,
    selectionCopyShow,
}
//Set selection highlight
function selectHightlightShow() {
    $("#luckysheet-cell-selected-boxs").show();
    $("#luckysheet-cell-selected-boxs #luckysheet-cell-selected").siblings(".luckysheet-cell-selected").remove();

    if (Store.luckysheet_select_save.length > 0) {
        for (let i = 0; i < Store.luckysheet_select_save.length; i++) {
            
            let r1 = Store.luckysheet_select_save[i].row[0],
                r2 = Store.luckysheet_select_save[i].row[1];
            let c1 = Store.luckysheet_select_save[i].column[0],
                c2 = Store.luckysheet_select_save[i].column[1];

            let rf, cf;
            if (Store.luckysheet_select_save[i].row_focus == null) {
                rf = r1;
            }
            else {
                rf = Store.luckysheet_select_save[i].row_focus;
            }

            if (Store.luckysheet_select_save[i].column_focus == null) {
                cf = c1;
            }
            else {
                cf = Store.luckysheet_select_save[i].column_focus;
            }

            let row = Store.visibledatarow[r2],
                row_pre = r1 - 1 == -1 ? 0 : Store.visibledatarow[r1 - 1];
            let col = Store.visibledatacolumn[c2],
                col_pre = c1 - 1 == -1 ? 0 : Store.visibledatacolumn[c1 - 1];

            let row_f = Store.visibledatarow[rf],
                row_pre_f = rf - 1 == -1 ? 0 : Store.visibledatarow[rf - 1];
            let col_f = Store.visibledatacolumn[cf],
                col_pre_f = cf - 1 == -1 ? 0 : Store.visibledatacolumn[cf - 1];

            // let margeset = menuButton.mergeborer(Store.flowdata, rf, cf);
            // if (!!margeset) {
            //     row_f = margeset.row[1];
            //     row_pre_f = margeset.row[0];

            //     col_f = margeset.column[1];
            //     col_pre_f = margeset.column[0];
            // }

            Store.luckysheet_select_save[i]["row"] = [r1, r2];
            Store.luckysheet_select_save[i]["column"] = [c1, c2];

            Store.luckysheet_select_save[i]["row_focus"] = rf;
            Store.luckysheet_select_save[i]["column_focus"] = cf;

            Store.luckysheet_select_save[i]["left"] = col_pre_f;
            Store.luckysheet_select_save[i]["width"] = col_f - col_pre_f - 1;
            Store.luckysheet_select_save[i]["top"] = row_pre_f;
            Store.luckysheet_select_save[i]["height"] = row_f - row_pre_f - 1;

            Store.luckysheet_select_save[i]["left_move"] = col_pre;
            Store.luckysheet_select_save[i]["width_move"] = col - col_pre - 1;
            Store.luckysheet_select_save[i]["top_move"] = row_pre;
            Store.luckysheet_select_save[i]["height_move"] = row - row_pre - 1;

            if (i == 0) {
                if (Store.luckysheet_select_save.length == 1) {
                    // if (browser.mobilecheck()) {//移动端
                    //     $("#luckysheet-cell-selected-boxs #luckysheet-cell-selected").css({
                    //         "left": Store.luckysheet_select_save[i]["left_move"],
                    //         "width": Store.luckysheet_select_save[i]["width_move"],
                    //         "top": Store.luckysheet_select_save[i]["top_move"],
                    //         "height": Store.luckysheet_select_save[i]["height_move"],
                    //         "display": "block",
                    //         "border": "1px solid #0188fb"
                    //     })
                    //         .find(".luckysheet-cs-draghandle")
                    //         .css("display", "block")
                    //         .end()
                    //         .find(".luckysheet-cs-fillhandle")
                    //         .css("display", "none")
                    //         .end()
                    //         .find(".luckysheet-cs-touchhandle")
                    //         .css("display", "block");
                    // }
                    // else {
                        $("#luckysheet-cell-selected-boxs #luckysheet-cell-selected").css({
                            "left": Store.luckysheet_select_save[i]["left_move"],
                            "width": Store.luckysheet_select_save[i]["width_move"],
                            "top": Store.luckysheet_select_save[i]["top_move"],
                            "height": Store.luckysheet_select_save[i]["height_move"],
                            "display": "block",
                            "border": "1px solid #0188fb"
                        })
                            .find(".luckysheet-cs-draghandle")
                            .css("display", "block")
                            .end()
                            .find(".luckysheet-cs-fillhandle")
                            .css("display", "block")
                            .end()
                            .find(".luckysheet-cs-touchhandle")
                            .css("display", "none");
                    // }
                }
                else {
                    $("#luckysheet-cell-selected-boxs #luckysheet-cell-selected").css({
                        "left": Store.luckysheet_select_save[i]["left_move"],
                        "width": Store.luckysheet_select_save[i]["width_move"],
                        "top": Store.luckysheet_select_save[i]["top_move"],
                        "height": Store.luckysheet_select_save[i]["height_move"],
                        "display": "block",
                        "border": "1px solid rgba(1, 136, 251, 0.15)"
                    })
                        .find(".luckysheet-cs-draghandle")
                        .css("display", "none")
                        .end()
                        .find(".luckysheet-cs-fillhandle")
                        .css("display", "none");
                }
            }
            else {
                $("#luckysheet-cell-selected-boxs").append('<div class="luckysheet-cell-selected" style="left: ' + Store.luckysheet_select_save[i]["left_move"] + 'px; width: ' + Store.luckysheet_select_save[i]["width_move"] + 'px; top: ' + Store.luckysheet_select_save[i]["top_move"] + 'px; height: ' + Store.luckysheet_select_save[i]["height_move"] + 'px; border: 1px solid rgba(1, 136, 251, 0.15); display: block;"></div>');
            }

            if (i == Store.luckysheet_select_save.length - 1) {
                //focus 取选区数组最后一个
                $("#luckysheet-cell-selected-focus").css({
                    "left": Store.luckysheet_select_save[i]["left"],
                    "width": Store.luckysheet_select_save[i]["width"],
                    "top": Store.luckysheet_select_save[i]["top"],
                    "height": Store.luckysheet_select_save[i]["height"],
                    "display": "block"
                });
                //行列数
                // luckysheet_count_show(
                //     Store.luckysheet_select_save[i]["left_move"],
                //     Store.luckysheet_select_save[i]["top_move"],
                //     Store.luckysheet_select_save[i]["width_move"],
                //     Store.luckysheet_select_save[i]["height_move"],
                //     [r1, r2],
                //     [c1, c2]
                // );
                //左上角选择区域框
                // formula.fucntionboxshow(rf, cf);
                //focus单元格数据验证
                // dataVerificationCtrl.cellFocus(rf, cf);
            }
        }
        // highlight row&column of selected cell
        selectTitlesShow(Store.luckysheet_select_save);


    }
}

// highlight row&column of selected cell
function selectTitlesShow(rangeArr) {
    // //行标题
    $("#luckysheet-rows-h-selected").empty();

    let row = rowLocationByIndex(rangeArr[0]["row"][1])[1],
        row_pre = rowLocationByIndex(rangeArr[0]["row"][0])[0];
    $("#luckysheet-rows-h-selected").append('<div class="luckysheet-rows-h-selected" style="top: ' + row_pre + 'px; height: ' + (row - row_pre - 1) + 'px; display: block; background-color: rgba(76, 76, 76, 0.1);"></div>');

    //列标题
    $("#luckysheet-cols-h-selected").empty();

    let col = colLocationByIndex(rangeArr[0]["column"][1])[1],
        col_pre = colLocationByIndex(rangeArr[0]["column"][0])[0];

    $("#luckysheet-cols-h-selected").append('<div class="luckysheet-cols-h-selected" style="left: ' + col_pre + 'px; width: ' + (col - col_pre - 1) + 'px; display: block; background-color: rgba(76, 76, 76, 0.1);"></div>');

}


// Dot line for copy cell area
function selectionCopyShow(range) {
    $("#luckysheet-selection-copy").empty();

    if (range == null) {
        range = Store.luckysheet_selection_range;
    }

    if (range.length > 0) {
        for (let s = 0; s < range.length; s++) {
            let r1 = range[s].row[0], r2 = range[s].row[1];
            let c1 = range[s].column[0], c2 = range[s].column[1];

            let row = Store.visibledatarow[r2],
                row_pre = r1 - 1 == -1 ? 0 : Store.visibledatarow[r1 - 1];
            let col = Store.visibledatacolumn[c2],
                col_pre = c1 - 1 == -1 ? 0 : Store.visibledatacolumn[c1 - 1];

            let copyDomHtml = '<div class="luckysheet-selection-copy" style="display: block; left: ' + col_pre + 'px; width: ' + (col - col_pre - 1) + 'px; top: ' + row_pre + 'px; height: ' + (row - row_pre - 1) + 'px;">' +
                '<div class="luckysheet-selection-copy-top luckysheet-copy"></div>' +
                '<div class="luckysheet-selection-copy-right luckysheet-copy"></div>' +
                '<div class="luckysheet-selection-copy-bottom luckysheet-copy"></div>' +
                '<div class="luckysheet-selection-copy-left luckysheet-copy"></div>' +
                '<div class="luckysheet-selection-copy-hc"></div>' +
                '</div>';
            $("#luckysheet-selection-copy").append(copyDomHtml);
        }
    }
}