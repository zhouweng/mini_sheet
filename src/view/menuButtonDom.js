
import {
    replaceHtml,
} from '../utils/util';
import {
    iconfontObjects,
} from '../utils/constant';

export {
    createButtonMenu,
    changeMenuButtonDom,
}
function changeMenuButtonDom(attr, foucsStatus) {
    if (attr == "bl") {
        if (foucsStatus != "0") {
            $("#luckysheet-icon-bold").addClass("luckysheet-toolbar-button-hover");
        } else {
            $("#luckysheet-icon-bold").removeClass("luckysheet-toolbar-button-hover");
        }
    } else if (attr == "it") {
        if (foucsStatus != "0") {
            $("#luckysheet-icon-italic").addClass("luckysheet-toolbar-button-hover");
        } else {
            $("#luckysheet-icon-italic").removeClass("luckysheet-toolbar-button-hover");
        }
    } else if (attr == "cl") {
        if (foucsStatus != "0") {
            $("#luckysheet-icon-strikethrough").addClass("luckysheet-toolbar-button-hover");
        } else {
            $("#luckysheet-icon-strikethrough").removeClass("luckysheet-toolbar-button-hover");
        }
    } else if (attr == "un") {
        if (foucsStatus != "0") {
            $("#luckysheet-icon-underline").addClass("luckysheet-toolbar-button-hover");
        } else {
            $("#luckysheet-icon-underline").removeClass("luckysheet-toolbar-button-hover");
        }
    } else if (attr == "fs") {
        let $menuButton = $("#luckysheet-icon-font-size-menuButton");
        let itemvalue = foucsStatus,
            $input = $("#luckysheet-icon-font-size input");
        focus($menuButton, itemvalue);
        $("#luckysheet-icon-font-size").attr("itemvalue", itemvalue);
        $input.val(itemvalue);
    } else if (attr == "ht") {
        let $menuButton = $("#luckysheet-icon-align-menu-menuButton");
        let itemvalue = "left";

        if (foucsStatus == "0") {
            itemvalue = "center";
        } else if (foucsStatus == "2") {
            itemvalue = "right";
        }

        focus($menuButton, itemvalue);

        // add iconfont
        const iconfontObject = iconfontObjects.align;

        let $icon = $("#luckysheet-icon-align").attr("type", itemvalue).find(".luckysheet-icon-img-container");
        $icon.removeAttr("class").addClass("luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-align-" + itemvalue + iconfontObject[itemvalue]);
        $menuButton.hide();
    } else if (attr == "vt") {
        let $menuButton = $("#luckysheet-icon-valign-menu-menuButton");
        let itemvalue = "bottom";

        if (foucsStatus == "1") {
            itemvalue = "top";
        } else if (foucsStatus == "0") {
            itemvalue = "middle";
        }

        focus($menuButton, itemvalue);

        // add iconfont
        const iconfontObject = iconfontObjects.align;

        let $icon = $("#luckysheet-icon-valign").attr("type", itemvalue).find(".luckysheet-icon-img-container");
        $icon.removeAttr("class").addClass("luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-valign-" + itemvalue + iconfontObject[itemvalue]);
        $menuButton.hide();
    } else if (attr == "mc") {
        let $menuButton = $("#luckysheet-icon-merge-menu-menuButton");
        let itemvalue = "all";
        if (foucsStatus == "1") {
            itemvalue = "cancel";
        }
        const iconfontObject = iconfontObjects.merge;
        let $icon = $("#luckysheet-icon-merge-button").attr("type", itemvalue).find(".luckysheet-icon-img-container");
        $icon.removeAttr("class").addClass("luckysheet-icon-img-container luckysheet-icon-img luckysheet-icon-merge-" + itemvalue + iconfontObject[itemvalue]);
        $menuButton.hide();
    }
}


function createButtonMenu(itemdata) {
    let itemset = "";
    let itemFontSize = '<div itemvalue="${value}" itemname="${name}" class="luckysheet-cols-menuitem ${sub} luckysheet-mousedown-cancel"><div class="luckysheet-cols-menuitem-content luckysheet-mousedown-cancel" style="padding: 3px 0px 3px 1px;"><span style="margin-right:3px;width:13px;display:inline-block;" class="icon luckysheet-mousedown-cancel"></span> ${name} <span class="luckysheet-submenu-arrow luckysheet-mousedown-cancel ${iconClass}" style="user-select: none;">${example}</span></div></div>';
    let splitLine = '<div class="luckysheet-menuseparator luckysheet-mousedown-cancel" role="separator"></div>';

    for (let i = 0; i < itemdata.length; i++) {
        let item = itemdata[i];

        if (item.value == "split") {
            itemset += splitLine;
        } else {
            if (item.example == "more") {
                itemset += replaceHtml(itemFontSize, {
                    "value": item.value,
                    "name": item.text,
                    "example": "",
                    "sub": "luckysheet-cols-submenu",
                    "iconClass": "iconfont luckysheet-iconfont-youjiantou"
                });

            } else {
                itemset += replaceHtml(itemFontSize, {
                    "value": item.value,
                    "name": item.text,
                    "example": item.example,
                    "sub": "",
                    "iconClass": ""
                });
            }
        }
    }

    return itemset;
}


function focus($obj, value) {

    $obj.find(".luckysheet-cols-menuitem").find("span.icon").html("");
    if (value == null) {
        $obj.find(".luckysheet-cols-menuitem").eq(0).find("span.icon").html('<i class="fa fa-check luckysheet-mousedown-cancel"></i>');
    } else {
        $obj.find(".luckysheet-cols-menuitem[itemvalue='" + value + "']").find("span.icon").html('<i class="fa fa-check luckysheet-mousedown-cancel"></i>');
    }
}
