
import {
    refreshCell,
} from './updateCell';
import {
    selectHightlightShow
} from './select';
import {
    menuButtonFocus,
} from '../controllers/menuButton';
import {
    mergeborder,
} from '../model/mergeborder';
import {
    getMouseClickedPos,
} from '../utils/location';
import Store from '../store';


export {
    highlightOneCell,
}

// hightlight one cell
function highlightOneCell(event) {
    // Get mouse's position from location.js
    let mouseClickPos = getMouseClickedPos(event);
    let row = mouseClickPos[0][1],
        row_pre = mouseClickPos[0][0],
        row_index = mouseClickPos[0][2];
    let col = mouseClickPos[1][1],
        col_pre = mouseClickPos[1][0],
        col_index = mouseClickPos[1][2];
    let row_index_ed = row_index,
        col_index_ed = col_index;
    //若点击单元格部分不在视图内
    if (col_pre < $("#luckysheet-cell-main").scrollLeft()) {
        $("#luckysheet-scrollbar-x").scrollLeft(col_pre);
    }

    if (row_pre < $("#luckysheet-cell-main").scrollTop()) {
        $("#luckysheet-scrollbar-y").scrollTop(row_pre);
    }

    Store.luckysheet_select_status = true;

    // let row_index_ed = row_index, col_index_ed = col_index;
    let mergeset = mergeborder(Store.flowdata, row_index, col_index);
    if (!!mergeset) {
        row = mergeset.row[1];
        row_pre = mergeset.row[0];
        row_index = mergeset.row[2];
        row_index_ed = mergeset.row[3];

        col = mergeset.column[1];
        col_pre = mergeset.column[0];
        col_index = mergeset.column[2];
        col_index_ed = mergeset.column[3];
    }

    //restore this point
    Store.luckysheet_select_save.length = 0;
    Store.luckysheet_select_save.push({
        "left": col_pre,
        "width": col - col_pre - 1,
        "top": row_pre,
        "height": row - row_pre - 1,
        "left_move": col_pre,
        "width_move": col - col_pre - 1,
        "top_move": row_pre,
        "height_move": row - row_pre - 1,
        "row": [row_index, row_index_ed],
        "column": [col_index, col_index_ed],
        "row_focus": row_index,
        "column_focus": col_index
    });
    Store.luckysheet_select_status = true;

    selectHightlightShow();
    menuButtonFocus(Store.flowdata, row_index, col_index);
    if (Store.luckysheetCellUpdate) {
        refreshCell(Store.luckysheetCellUpdate[0], Store.luckysheetCellUpdate[1]);
    }
}