import { 
    gridHTML, 
    inputHTML,
    flow,
    rightclickHTML,
    menuToolBar,
} from '../utils/constant';
import { rhchInit } from '../model/rhchInit';
import {
    replaceHtml
} from '../utils/util';
import Store from '../store';

export {
    luckysheetcreatedom
}

function luckysheetcreatedom(colwidth, rowheight, data, menu, title) {

    let gh = gridHTML();
    gh = replaceHtml(gh, { "menu": menuToolBar() }); 
    let flowHTML = flow;
    if(Store.config == null){
        Store.config = {};
    }

    rhchInit(rowheight, colwidth);

    let flowstr = replaceHtml('<div id="luckysheetcoltable_0" class="luckysheet-cell-flow-col"> <div id ="luckysheet-sheettable_0" class="luckysheet-cell-sheettable" style="height:${height}px;width:${width}px;"></div></div>', {
        "height": Store.rh_height,
        "width": Store.ch_width - 1
    });
    flowHTML = replaceHtml(flowHTML, {
        "width": Store.ch_width,
        "flow": flowstr,
        "index": 0
    });
    gh = replaceHtml(gh, {
        "flow": flowHTML
    });

    $("#" + Store.container).append(gh);
    $("#luckysheet-scrollbar-x div").width(Store.ch_width);
    $("#luckysheet-scrollbar-y div").height(Store.rh_height + Store.columnHeaderHeight - Store.cellMainSrollBarSize - 3);
    $("#luckysheet-rows-h").width((Store.rowHeaderWidth-1.5));
    $("#luckysheet-cols-h-c").height((Store.columnHeaderHeight-1.5));
    $("#luckysheet-left-top").css({width:Store.rowHeaderWidth-1.5, height:Store.columnHeaderHeight-1.5});

    $("body").append(inputHTML);
    $("body").append(rightclickHTML());
    

}