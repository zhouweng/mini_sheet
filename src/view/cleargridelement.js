import {
    clearcopy
} from '../controllers/selection';

export {
    cleargridelement,
    clearmenuelement,
} ;

function cleargridelement(event) {
    $("#luckysheet-rightclick-menu").hide();

    $("#luckysheet-cell-selected-boxs .luckysheet-cell-selected").hide();
    $("#luckysheet-cols-h-selected .luckysheet-cols-h-selected").hide();
    $("#luckysheet-rows-h-selected .luckysheet-rows-h-selected").hide();

    $("#luckysheet-cell-selected-focus").hide();
    $("#luckysheet-rows-h-hover").hide();
    $("#luckysheet-selection-copy .luckysheet-selection-copy").hide();
    $("#luckysheet-cols-menu-btn").hide();
    $("#luckysheet-row-count-show, #luckysheet-column-count-show").hide();
    if (!event) {
        clearcopy(event);
    }

}

function clearmenuelement() {
    $("#luckysheet-rightclick-menu").hide();
    $("#luckysheet-icon-font-size-menuButton").hide();
    $("#luckysheet-icon-merge-menu-menuButton").hide();
    $("#luckysheet-icon-align-menu-menuButton").hide();
    $("#luckysheet-icon-valign-menu-menuButton").hide();
    $("#luckysheet-icon-cell-color-menu-menuButton").hide();
    
}
