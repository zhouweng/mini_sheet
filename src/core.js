import defaultSetting from './config.js';
import Store from './store';
import {initialjfFile} from './controllers/sheetmanage';
import {luckysheetHandler} from './controllers/handler';
import {keyboardInitial} from './controllers/keyboard';
import {initialMenuButton} from './controllers/menuButton';
import {rowColumnOperationInitial} from './controllers/rowColumnOperation';
import {common_extend} from './utils/util';
import {luckysheetloadingHTML} from './utils/constant';

export {
    luckysheet
}

let luckysheet = {};

//创建luckysheet表格
luckysheet.create = function (setting) {
    // destroy()
    // mix user's and system default parameter(user's setting overide default setting)
    let extendsetting = common_extend(defaultSetting, setting);

    let menu = extendsetting.menu,
        title = extendsetting.title;

    let container = extendsetting.container;
    Store.container = container;
    Store.luckysheetfile = extendsetting.data;
    Store.defaultcolumnNum = extendsetting.column;
    Store.defaultrowNum = extendsetting.row;

    // //loading
    $("#" + container).append(luckysheetloadingHTML());

    initialjfFile(menu, title);
    luckysheetHandler(); //Overall dom initialization
    keyboardInitial();//Keyboard operate initialization
    initialMenuButton(); // toolbar button initialization
    rowColumnOperationInitial();//row and coloumn operate initialization
}
